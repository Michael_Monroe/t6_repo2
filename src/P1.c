#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "P1.h"

#define alive 0
#define dead 1
#define simm 2

globals getGlobals(int argc, char const* argv[]){
  globals G0;
  //Default values:
  G0.infected = 0.0;
  G0.immune = 0.0;
  G0.lifespan = 50 * 52;
  G0.chance_reproduce = 1;
  G0.carrying_capacity = 300;
  G0.immunity_duration = 52;
  G0.infectiousness = 65;
  G0.duration = 20;
  G0.chance_recover = 75;
  G0.number_people = 150;
  G0.ticks = 100;

  for (int i = 1; i < argc; i = i + 2) {
    if (strcmp("-lsp", argv[i]) == 0) {
      G0.lifespan = atoi(argv[i+1]);
    }
    else if (strcmp("-cr", argv[i]) == 0) {
      G0.chance_reproduce = atoi(argv[i+1]);
    }
    else if (strcmp("-cc", argv[i]) == 0) {
      G0.carrying_capacity = atoi(argv[i+1]);
    }
    else if (strcmp("-imd", argv[i]) == 0) {
      G0.immunity_duration = atoi(argv[i+1]);
    }
    else if (strcmp("-inf", argv[i]) == 0) {
      G0.infectiousness = atoi(argv[i+1]);
    }
    else if (strcmp("-dt", argv[i]) == 0) {
      G0.duration = atoi(argv[i+1]);
    }
    else if (strcmp("-rec", argv[i]) == 0) {
      G0.chance_recover = atoi(argv[i+1]);
    }
    else if (strcmp("-np", argv[i]) == 0) {
      G0.number_people = atoi(argv[i+1]);
    }
    else if (strcmp("-tck", argv[i]) == 0) {
      G0.ticks = atoi(argv[i+1]);
    }

  }

  if(G0.number_people < 10){

    G0.number_people = 10;
    printf("Number_people is under range, it will be set as the minimun permissible value.");

  }
  else if(G0.number_people > G0.carrying_capacity){

    G0.number_people = G0.carrying_capacity;
    printf("Number_people is above range, it will be set as the maximum permissible value.");

  }

  return G0;
}


void get_sick(turtles* T1){
    T1->sick = true;
    T1->remaining_immunity = 0;
}

void get_healthy(turtles* T1){
  T1->sick = false;
  T1->remaining_immunity = 0;
  T1->sick_time = 0;
}

void become_immune(turtles* T1, globals* G0){
  T1->sick = false;
  T1->sick_time = 0;
  T1->remaining_immunity = G0->immunity_duration;
}

int get_older(turtles* T1, globals* G0){

  int status = alive;
  T1->age = T1->age + 1;
  if(T1->age > G0->lifespan){
        status = dead;
  }
  if(T1->remaining_immunity > 0){
        T1->remaining_immunity = T1->remaining_immunity - 1;
  }
  if(T1->sick){
        T1->sick_time = T1->sick_time + 1;
  }
  return status;
}


void get_move(turtles* T1){

    //srand(time(0));
    int x = rand() % 151;
    int y = rand() % 151;

    if(x >= 0 && x < 50){
        if(T1->posX != 0){
            T1->posX = T1->posX - 1;
        }
        else{
            T1->posX = 35;
        }
    }

    if(x >= 50 && x < 100){
        if(T1->posX != 35){
            T1->posX = T1->posX + 1;
        }
        else{
            T1->posX = 0;
        }
    }

    if(y >= 0 && y < 50){
        if(T1->posY != 0){
            T1->posY = T1->posY - 1;
        }
        else{
            T1->posY = 35;
        }
    }

    if(y >= 50 && y < 100){
        if(T1->posY != 35){
            T1->posY = T1->posY + 1;
        }
        else{
            T1->posY = 0;
        }
    }

}


int infect(turtles* T1, globals* G0){

    int stat = alive;
    if(!T1->sick && T1->remaining_immunity == 0){

        //srand(time(0));
        int i = rand() % 100;
        if(i < G0->infectiousness){
            get_sick(T1);
            stat = dead;
        }
    }
    return stat;
}


int recover_or_die(turtles* T1, globals* G0){

    int status = alive;

    if (T1->sick_time > G0->duration){
        //srand(time(0));
        int i = rand() % 100;
        if(i < G0->chance_recover){
            become_immune(T1, G0);
            status = simm;
        }
        else{
            status = dead;
        }
    }
    return status;
}

int reproduce(int count_turtles, turtles* T1, globals* G0){

    int stat = dead;
    //srand(time(0));
    int i = rand() % 100;
    if(count_turtles < G0->carrying_capacity && i < G0->chance_reproduce){
        turtles* baby = born_turtle(T1);
        if(baby != NULL){
            stat = alive;
        }
    }
    return stat;
}

void kill_turtle(turtles* C1, turtles* C0){

    if(C0 != NULL){
    C0->Tp1 = C1->Tp1;
    }
    free(C1);
}

turtles* born_turtle(turtles* C0){ //C0 = mother

    turtles* C1 = (turtles*) malloc(sizeof(turtles));
    if(C1 == NULL){
        printf("No se pudo alocar memoria.");
        return NULL;
    }
    C1->Tp1 = C0->Tp1;
    C1->Tm1 = C0;
    C0->Tp1 = C1;

    C1->posX = C0->posX;
    C1->posY = C0->posY;
    get_move(C1);

    C1->sick = false;
    C1->remaining_immunity = 0;
    C1->sick_time = 0;
    C1->age = 1;

    return C1;
}

turtles* create_turtle(turtles* C0, globals* G0){

    turtles* C1 = (turtles*) malloc(sizeof(turtles));
    if(C1 == NULL){
        printf("No se pudo alocar memoria.");
        return NULL;
    }
    C1->Tp1 = NULL;
    C1->Tm1 = C0;
    C0->Tp1 = C1;

    //srand(time(0));
    C1->posX = rand() % 36;
    C1->posY = rand() % 36;

    C1->sick = false;
    C1->remaining_immunity = 0;
    C1->sick_time = 0;
    C1->age = rand() % G0->lifespan;

    return C1;
}


void insertTurtle(turtles* T0, globals* G0, int s){
    turtles* temp;
    turtles* T1; // = (turtles*) malloc(sizeof(turtles));
    temp = T0;

    while(temp->Tp1 != NULL)
        temp = temp->Tp1;

    T1 = create_turtle(temp, G0);
    if(s==0){
        T1->sick = true;
    }
    //temp->Tp1 = T1;

}

