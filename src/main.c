#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "P1.h"

#define alive 0
#define dead 1
#define simm 2

int main(int argc, char const *argv[]) {

  //Initialize the simulation parameters and statistics.
    globals G0 = getGlobals(argc, argv);
    int tot_p = 0;
    int tot_s = 0;
    int tot_i = 0;

    turtles T0;
    T0.Tp1 = NULL;

    srand(time(0));


    char* fileName = "output/outfile.txt";
    FILE* file_ptr;
    file_ptr = fopen(fileName, "w");
    fprintf(file_ptr, "tck\t|\t tot_p\t|\t tot_s\t|\t tot_i\t|\t inf\t|\t imm\t|\n");



    for(int i = 0; i < 11; i++){

        insertTurtle(&T0, &G0, 0);
        //printf("Todo bien\n");
    }
    tot_p = 10;
    tot_s = 10;


    for(int i = 10; i < G0.number_people; i++){

        insertTurtle(&T0, &G0, 1);
        tot_p = tot_p + 1;
    }



    for(int tck = 0; tck < G0.ticks; tck++){

        turtles* temp = T0.Tp1;
        while(temp->Tp1 != NULL){

            if(temp->remaining_immunity == 1){
                tot_i = tot_i - 1;
            }
            int stat = get_older(temp, &G0);

            if(stat == dead){

                turtles* aux = temp;
                temp = temp->Tp1;
                kill_turtle(aux, aux->Tm1);
                tot_p = tot_p - 1;
            }
            else{
                temp = temp->Tp1;
            }
        }

        temp = T0.Tp1;
        while(temp->Tp1 != NULL){

            get_move(temp);
            temp = temp->Tp1;
        }

        temp = T0.Tp1;
        while(temp->Tp1 != NULL){

            if(temp->sick){
                int stat = recover_or_die(temp, &G0);

                if(stat == dead){

                    turtles* aux = temp;
                    temp = temp->Tp1;
                    kill_turtle(aux, aux->Tm1);
                    tot_p = tot_p - 1;
                }
                else if(stat == simm){
                    tot_i = tot_i + 1;
                    tot_s = tot_s - 1;
                    temp = temp->Tp1;
                }
                else{
                    temp = temp->Tp1;
                }
            }
            else{
                temp = temp->Tp1;
            }
        }

        temp = T0.Tp1;
        while(temp->Tp1 != NULL){

            if(temp->sick){
                turtles* temp2 = T0.Tp1;
                int posX = temp->posX;
                int posY = temp->posY;
                while(temp2->Tp1 != NULL){

                    //printf("%i-%i___%i-%i\n", posX, posY, temp2->posX, temp2->posY);
                    if(temp2->posX == posX && temp2->posY == posY && temp2 != temp){
                            int stat = infect(temp2, &G0);
                            if(stat == dead){
                                tot_s = tot_s + 1;
                                //printf("%i-%i___%i-%i\n", posX, posY, temp2->posX, temp2->posY);
                            }
                    }
                    temp2 = temp2->Tp1;
                }
            }
            else{
                int stat = reproduce(tot_p, temp, &G0);
                if(stat == alive){
                    tot_p = tot_p + 1;
                }
            }
            temp = temp->Tp1;
        }

        if (tot_p > 0){
            G0.infected = (float) tot_s/tot_p * 100;
            G0.immune = (float) tot_i/tot_p * 100;
        }

       fprintf(file_ptr, "%i\t|\t %i\t|\t %i\t|\t %i\t|\t %f\t|\t %f\t|\n", tck, tot_p, tot_s, tot_i, G0.infected, G0.immune);
    }

    turtles* temp = &T0;
    while(temp->Tp1 != NULL){

        turtles* aux = temp;
        temp = temp->Tp1;
        kill_turtle(aux, aux->Tm1);
    }

    fclose(file_ptr);
    return 0;

}
