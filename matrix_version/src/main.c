#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "P1.h"

#define alive 0
#define dead 1
#define simm 2

int main(int argc, char const *argv[]) {

  //Initialize the simulation parameters and statistics.
    globals G0 = getGlobals(argc, argv);
    int tot_p = 0;
    int tot_s = 0;
    int tot_i = 0;

    srand(time(0));


    char* fileName = "output/outfile.txt";
    FILE* file_ptr;
    file_ptr = fopen(fileName, "w");
    fprintf(file_ptr, "tck\t|\t tot_p\t|\t tot_s\t|\t tot_i\t|\t inf\t|\t imm\t|\n");



    cell world[100][100];

    for(int y = 0; y<100; y++){

        for(int x = 0; x<100; x++){

                world[y][x].T0 = NULL;
                world[y][x].valid = false;
        }
    }

    for(int i = 0; i<11; i++){

        bool keep = true;
        while(keep){
            int x = rand() % 100;
            int y = rand() % 100;
            if(!world[y][x].valid){

                world[y][x].T0 = create_turtle(&G0);
                world[y][x].valid = true;
                get_sick(world[y][x].T0);
                keep = false;
            }
        }
    }

    tot_s = 10;

    for(int i = 0; i<G0.number_people-9; i++){

        bool keep = true;
        while(keep){
            int x = rand() % 100;
            int y = rand() % 100;
            if(!world[y][x].valid){

                world[y][x].T0 = create_turtle(&G0);
                world[y][x].valid = true;
                keep = false;
            }
        }
    }

    tot_p = G0.number_people;

    for(int tck = 0; tck < G0.ticks+1; tck++){


        // get_older turtles
        for(int y = 0; y<100; y++){

            for(int x = 0; x<100; x++){

                if(world[y][x].valid){

                    if(world[y][x].T0->remaining_immunity == 1){
                        tot_i--;
                    }
                    int stat = get_older(world[y][x].T0, &G0);
                    if(stat == dead){
                        if(world[y][x].T0->sick){
                            tot_s--;
                        }
                        kill_turtle(world[y][x].T0);
                        tot_p--;
                        world[y][x].T0 = NULL;
                        world[y][x].valid = false;
                        printf("Dead\n");
                    }
                }
            }
        }


        // move turtles
        for(int y = 0; y<100; y++){

            for(int x = 0; x<100; x++){

                if(world[y][x].valid){

                    bool keep = true;
                    while(keep){

                        int new_x;
                        int new_y;
                        new_x = x + get_move();
                        new_y = y + get_move();
                        if(new_x < 100 && new_x > -1 && new_y < 100 && new_y > -1){
                            if(!world[new_y][new_x].valid){
                                world[new_y][new_x].T0 = world[y][x].T0;
                                world[new_y][new_x].valid = true;
                                world[y][x].valid = false;
                                keep = false;
                            }
                            else if(new_x == x && new_y == y){
                                keep = false;
                            }
                        }
                    }
                }
            }
        }

        //recover or die
        for(int y = 0; y<100; y++){

            for(int x = 0; x<100; x++){

                if(world[y][x].valid){

                    if(world[y][x].T0->sick){
                        int stat = recover_or_die(world[y][x].T0, &G0);
                        if(stat == dead){

                            kill_turtle(world[y][x].T0);
                            tot_p--;
                            tot_s--;
                            world[y][x].T0 = NULL;
                            world[y][x].valid = false;
                            printf("Dead\n");
                        }
                        else if(stat == simm){
                            tot_s--;
                            tot_i++;
                            printf("Immune\n");
                        }
                    }
                }
            }
        }

        // Infect or reproduce
        for(int y = 0; y<100; y++){

            for(int x = 0; x<100; x++){

                if(world[y][x].valid){

                    if(world[y][x].T0->sick){

                        if(x+1 < 100 && y+1 < 100){
                            if(world[y+1][x+1].valid){
                                int stat = infect(world[y+1][x+1].T0, &G0);
                                if(stat == dead){
                                        tot_s++;
                                }
                            }
                        }

                        if(y+1 < 100){
                            if(world[y+1][x].valid){
                                int stat = infect(world[y+1][x].T0, &G0);
                                if(stat == dead){
                                    tot_s++;
                                }
                            }
                        }

                        if(x-1 > -1 && y+1 < 100){
                            if(world[y+1][x-1].valid){
                                int stat = infect(world[y+1][x-1].T0, &G0);
                                if(stat == dead){
                                    tot_s++;
                                }
                            }
                        }

                        if(x+1 < 100){
                            if(world[y][x+1].valid){
                                int stat = infect(world[y][x+1].T0, &G0);
                                if(stat == dead){
                                        tot_s++;
                                }
                            }
                        }

                        if(x-1 > -1){
                            if(world[y][x-1].valid){
                                int stat = infect(world[y][x-1].T0, &G0);
                                if(stat == dead){
                                    tot_s++;
                                }
                            }
                        }

                        if(x+1 < 100 && y-1 >-1){
                            if(world[y-1][x+1].valid){
                                int stat = infect(world[y-1][x+1].T0, &G0);
                                if(stat == dead){
                                        tot_s++;
                                }
                            }
                        }

                        if(y-1 >-1){
                            if(world[y-1][x].valid){
                                int stat = infect(world[y-1][x].T0, &G0);
                                if(stat == dead){
                                    tot_s++;
                                }
                            }
                        }

                        if(x-1 > -1 && y-1 >-1){
                            if(world[y-1][x-1].valid){
                                int stat = infect(world[y-1][x-1].T0, &G0);
                                if(stat == dead){
                                    tot_s++;
                                }
                            }
                        }
                    }
                    else {

                        int b_x = -1;
                        int b_y = -1;

                        if(x+1 < 100 && y+1 < 100){
                            if(!world[y+1][x+1].valid){
                                b_x = x+1;
                                b_y = y+1;
                            }
                        }

                        if(y+1 < 100){
                            if(!world[y+1][x].valid){
                                b_x = x;
                                b_y = y+1;
                            }
                        }

                        if(x-1 > -1 && y+1 < 100){
                            if(!world[y+1][x-1].valid){
                                b_x = x-1;
                                b_y = y+1;
                            }
                        }

                        if(x+1 < 100){
                            if(!world[y][x+1].valid){
                                b_x = x+1;
                                b_y = y;
                            }
                        }

                        if(x-1 > -1){
                            if(!world[y][x-1].valid){
                                b_x = x-1;
                                b_y = y;
                            }
                        }

                        if(x+1 < 100 && y-1 >-1){
                            if(!world[y-1][x+1].valid){
                                b_x = x+1;
                                b_y = y-1;
                            }
                        }

                        if(y-1 >-1){
                            if(!world[y-1][x].valid){
                                b_x = x;
                                b_y = y-1;
                            }
                        }

                        if(x-1 > -1 && y-1 >-1){
                            if(!world[y-1][x-1].valid){
                                b_x = x-1;
                                b_y = y-1;
                            }
                        }

                        if(b_x != -1 && b_y != -1){
                            world[b_y][b_x].T0 = reproduce(tot_p, &G0);
                            if(world[b_y][b_x].T0 != NULL){
                                tot_p++;
                                world[b_y][b_x].valid = true;
                                printf("Born\n");
                            }
                        }
                    }
                }
            }
        }

        if (tot_p > 0){
            G0.infected = (float) tot_s/tot_p * 100;
            G0.immune = (float) tot_i/tot_p * 100;
        }

        printf("---%i---\n", tck);
        fprintf(file_ptr, "%i\t|\t %i\t|\t %i\t|\t %i\t|\t %f\t|\t %f\t|\n", tck, tot_p, tot_s, tot_i, G0.infected, G0.immune);

    }


    for(int y = 0; y<100; y++){

        for(int x = 0; x<100; x++){

            if(world[y][x].valid){

                kill_turtle(world[y][x].T0);
                world[y][x].valid = false;
            }
        }
    }


    fclose(file_ptr);
    return 0;

}

